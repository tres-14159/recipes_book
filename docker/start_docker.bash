#!/bin/bash

path_script="$( cd "$(dirname "$0")" ; pwd -P )";
path_working="$(pwd)";

cd $path_script;


docker-compose -p recipes_book up --build -d

cd $path_working;
