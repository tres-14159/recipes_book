#!/bin/bash

working_path="$(pwd)";

docker rm $(docker ps -a -f "name=recipes_book_" 2>/dev/null) -f -v 2>/dev/null;
docker volumen prune -f 2>/dev/null

cd docker

docker-compose -p recipes_book -f docker-compose.yml -f docker-compose-test.yml up --build -d

cd $working_path

docker exec recipes_book_httpd phpunit
docker exec recipes_book_httpd cat logs/coverage.txt
