# Recipes Book

[![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![Code Climate](https://codeclimate.com/github/mdtrooper/recipes_book/badges/gpa.svg)](https://codeclimate.com/github/mdtrooper/recipes_book)
[![Build Status](https://travis-ci.org/mdtrooper/recipes_book.svg?branch=master)](https://travis-ci.org/mdtrooper/recipes_book)
[![Coverage Status](https://coveralls.io/repos/github/mdtrooper/recipes_book/badge.svg?branch=master)](https://coveralls.io/github/mdtrooper/recipes_book?branch=master)

A tiny PHP web for to save own recipes
